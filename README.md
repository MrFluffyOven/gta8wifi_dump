# gta8wifixx-user 14 UP1A.231005.007 X200XXU3DXB3 release-keys
- manufacturer: samsung
- platform: ums512
- codename: gta8wifi
- flavor: gta8wifixx-user
- release: 14
- id: UP1A.231005.007
- incremental: X200XXU3DXB3
- tags: release-keys
- fingerprint: samsung/gta8wifixx/gta8wifi:11/RP1A.200720.012/X200XXU3DXB3:user/release-keys
- is_ab: false
- brand: samsung
- branch: gta8wifixx-user-14-UP1A.231005.007-X200XXU3DXB3-release-keys
- repo: samsung_gta8wifi_dump
